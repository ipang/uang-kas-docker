# Containerized Express & MySQL

It's cloned from
> [github.com/hako-975 - aplikasi uang kas dengan php native](https://github.com/hako-975/aplikasi-uang-kas-dengan-php-native)

## Project setup
```
docker-compose up
```
then you have to import the table, IDK why entrypoint not working, for now we do it manually
```
docker exec -i express-rest-mysql mysql -u root -pdramaterus data < data.sql
```
#
### Use docker-swarm
Create mysql service
```
docker service create --name uang-kas-mysql \
--publish 3306:3306 \
--network traefik-public \
--replicas 1 \
--mount 'type=volume,source=uang-kas-mysql-vol,destination=/var/lib/mysql' \
-e MYSQL_ROOT_PASSWORD=19second \
-e MYSQL_USER=eren \
-e MYSQL_PASSWORD=wanttorevenge \
-e MYSQL_DATABASE=uang_kas
```
Then, import database
```
docker exec -i uang-kas-mysql mysql -u root -p19second uang_kas < uang_kas.sql 
```
Next, create uang kas swarm service
```
docker stack deploy -c docker-stack.yml uang-kas 
```
**Don't forget to change domain  at docker-stack.yml**

#
# aplikasi-uang-kas-dengan-php-native
Aplikasi Sederhana untuk mengelola uang kas sekaligus pengeluarannya. 
Aplikasi ini hanya dapat menghandle satu kelas saja.
Meskipun begitu, tapi aplikasi ini sangat praktis dan tetap optimal dengan bisa menghandle uang kas dalam beberapa tahun serta menyediakan fitur-fitur yang sangat dibutuhkan seperti: riwayat pembayaran dan pengeluaran, laporan hasil perbulan dan lain-lain. 

Cara memasangnya:
1. Download file ini
2. Ekstrak pada htdocs/
3. Buka phpmyadmin
4. Buat database dengan nama: uang_kas
5. Import file uang_kas.sql pada folder yang di ekstrak tadi
6. Buka localhost/uang_kas
7. Selesai

Akun:

Administrator:

❥ Username: andri123

❥ Password: 123456


Bendahara:

❥ Username: andre123

❥ Password: 123456


Guru:

❥ Username: annisa321

❥ Password: 123456


Fitur:
- Hak akses
- Mengelola Uang Kas Perbulan
- Riwayat Pembayaran
- Mengelola Pengeluaran
- Riwayat Pengeluaran
- Ganti Password
- Dan Lainnya
